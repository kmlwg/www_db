# About
It's a web page about books. A user can post articles, add new books, authors, comment on them and rate them and add to read list.

### Stack
1. Database: MariaDB
2. Backend: PHP
3. Frontend: HTML + CSS + JS