-- ---------------------------------------------------------------------------------------------------------
-- AUTHORS
-- ---------------------------------------------------------------------------------------------------------

INSERT INTO author (firstname, surname, date_born, date_death, country)
    VALUES ('Agatha', 'Christie', '1890', '1976','Anglia');

INSERT INTO author (firstname, surname, date_born, country)
    VALUES ('Stephen', 'King', '1947', 'USA');

INSERT INTO author (firstname, surname, date_born, country)
    VALUES ('Remigiusz', 'Mróz', '1987', 'Polska');

-- --------------------------------------------------------------------------------------------------------
-- BOOKS
-- --------------------------------------------------------------------------------------------------------

INSERT INTO book (title_pl, title_ori, isbn, year_published, page_number, publisher, rating, genre)
    VALUES ('Morderstwo w Orient Expressie',
            'Murder on the Orient Express',
            '9788327157782',
            '2017',
            264,
            'Wydawnictwo Dolnośląskie',
            9.8,
            'kryminał');

INSERT INTO author_book (author_id, book_id) VALUES (1,1);

INSERT INTO book (title_pl, title_ori, isbn, year_published, page_number, publisher, rating, genre)
VALUES ('Boże Narodzenie Herkulesa Poirot',
        'Hercule Poirot\'s Christmas',
        '8370751067',
        '1992',
        0,
        'Phantom Press International',
        7.0,
        'kryminał');

INSERT INTO author_book (author_id, book_id) VALUES (1,2);

INSERT INTO book (title_pl, title_ori, isbn, year_published, page_number, publisher, rating, genre)
VALUES ('Cyganka',
        'The gypsy',
        '8389200538',
        '2005',
        0,
        'Phantom Press International',
        7.6,
        'kryminał');

INSERT INTO author_book (author_id, book_id) VALUES (1,3);