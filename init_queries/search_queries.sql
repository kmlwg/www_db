-- Search by author surname
-- -------------------------
SELECT title_pl
FROM book b
       JOIN author_book ab ON b.book_id = ab.book_id
       JOIN author a ON a.author_id = ab.author_id
WHERE surname = 'Christie';