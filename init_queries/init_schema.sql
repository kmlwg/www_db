CREATE SCHEMA IF NOT EXISTS `booktest` 
DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `booktest` ;

-- -----------------------------------------------------
-- Table `booktest`.`book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `booktest`.`book` (
    `book_id`        INT NOT NULL AUTO_INCREMENT,
    `title_pl`       VARCHAR(45) NULL,
    `title_ori`      VARCHAR(45) NOT NULL,
    `isbn`           CHAR(13) NOT NULL,
    `year_published` INT NOT NULL,
    `page_number`    INT NULL DEFAULT 0,
    `publisher`      VARCHAR(45) NULL,
    `rating`         FLOAT NULL DEFAULT 0,
    `genre`          VARCHAR(45) NULL,
    PRIMARY KEY (`book_id`)
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `booktest`.`usern`
-- -----------------------------------------------------


CREATE TABLE IF NOT EXISTS `booktest`.`usern` (
  `user_id`       INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `username`      VARCHAR(10) NOT NULL,
  `password`      CHAR(64) NOT NULL,
  `email`         VARCHAR (20) NOT NULL,
  `date_created`  DATETIME NOT NULL,
  `date_logged`   DATETIME
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `booktest`.`user_book` (
  `user_id` INT NOT NULL,
  `book_id` INT NOT NULL,
  PRIMARY KEY(`user_id`, `book_id`),
  CONSTRAINT `fk_user_user_book`
    FOREIGN KEY(`user_id`)
    REFERENCES usern(user_id)
    ON DELETE CASCADE,
  CONSTRAINT `fk_book_user_book`
    FOREIGN KEY(`book_id`)
    REFERENCES book(book_id)
    ON DELETE CASCADE
) ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `booktest`.`author`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `booktest`.`author` (
    `author_id`  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `firstname`  VARCHAR(45) NOT NULL,
    `surname`    VARCHAR(45) NOT NULL,
    `date_born`  INT NOT NULL,
    `date_death` INT NULL,
    `country`    VARCHAR(45) NOT NULL
) ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `booktest`.`author_book`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `booktest`.`author_book` (
    `author_id` INT NOT NULL,
    `book_id`   INT NOT NULL,

    PRIMARY KEY(`author_id`,`book_id`),
    CONSTRAINT `fk_author_author_book`
        FOREIGN KEY(`author_id`) 
        REFERENCES author (author_id)
        ON DELETE CASCADE,
    CONSTRAINT `fk_book_author_book`
        FOREIGN KEY(`book_id`) 
        REFERENCES book (book_id) 
        ON DELETE CASCADE
    /*
    CONSTRAINT pk_author_book
        PRIMARY KEY(`author_id`,`book_id`)
    */
) ENGINE = InnoDB;
-- -----------------------------------------------------
-- Table `booktest`.`comment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `booktest`.`comment` (
    `comment_id`   INT NOT NULL AUTO_INCREMENT,
    `content`         TEXT NOT NULL,
    `date_created` DATETIME NOT NULL,
    
    `user_id`               INT NOT NULL,
    `book_id`               INT NOT NULL,
    
    PRIMARY KEY (`comment_id`, `user_id`),
    CONSTRAINT `fk_comment_usern`
        FOREIGN KEY (`user_id`)
        REFERENCES `booktest`.`usern` (`user_id`)
        ON DELETE CASCADE,
    CONSTRAINT `fk_comment_book`
        FOREIGN KEY (`book_id`)
        REFERENCES `booktest`.`book` (`book_id`)
        ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `booktest`.`articles` (
  `article_id`      INT NOT NULL AUTO_INCREMENT,
  `title`           VARCHAR(50),
  `content`            TEXT NOT NULL,
  `date_created`    DATETIME NOT NULL,
  `user_id`         INT NOT NULL,

  PRIMARY KEY (`article_id`, `user_id`),
  CONSTRAINT `fk_articles_usern`
    FOREIGN KEY (`user_id`)
    REFERENCES `booktest`.`usern` (`user_id`)
    ON DELETE CASCADE
) ENGINE = InnoDB;