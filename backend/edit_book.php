<?php

require "connect.php";
session_start();
$pdo = getDB();

$titlePl = $titleOri = $isbn = $publisher = $genre = "";
$year = $pages = $bookID = 0;
$book_err =  $db_error = "";





$sql = "UPDATE book 
        SET title_pl = :titlePl, title_ori = :titleOri, isbn = :isbn, year_published = :year,
            page_number = :pages, publisher = :publisher, genre = :genre
        WHERE book_id = :bookID";
if ($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":titlePl",   $titlePl,   PDO::PARAM_STR);
    $stmt->bindParam(":titleOri",  $titleOri,  PDO::PARAM_STR);
    $stmt->bindParam(":isbn",      $isbn,      PDO::PARAM_STR);
    $stmt->bindParam(":year",      $year,      PDO::PARAM_INT);
    $stmt->bindParam(":pages",     $pages,     PDO::PARAM_INT);
    $stmt->bindParam(":publisher", $publisher, PDO::PARAM_STR);
    $stmt->bindParam(":genre",     $genre,     PDO::PARAM_STR);
    $stmt->bindParam(":bookID",    $bookID,    PDO::PARAM_INT);
 
    $titlePl   = htmlspecialchars($_POST["titlePl"]);
    $titleOri  = htmlspecialchars($_POST["titleOri"]);
    $isbn = htmlspecialchars(trim($_POST["isbn"]));
    $year      = htmlspecialchars($_POST["year"]);
    $publisher = htmlspecialchars($_POST["publisher"]);
    $pages     = htmlspecialchars($_POST["pages"]);
    $genre     = htmlspecialchars($_POST["genre"]);
    $bookID    = $_SESSION["bookID"];
    if ($stmt->execute()) {          
        unset($stmt);
        unset($pdo);
        echo "BITH";
        header("location: ../index.php?page=editBook&bookID=$bookID");
    } else {
        $db_error = "Failed to execute inserting book";
    }
} else {
    $db_error = "Failed to bind prepare stmt";
}
echo $db_error;