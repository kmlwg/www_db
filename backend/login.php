<?php
// Initialize the session
//session_start();

// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
    header("location: welcome.php");
    exit;
}

// Include config file
require_once "connect.php";

$pdo = getDB();

// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";

// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST") {

    // Check if username is empty
    if(empty(trim($_POST["username"]))) {
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }

    // Check if password is empty
    if(empty(trim($_POST["pswd"]))) {
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["pswd"]);
    }

    // Validate credentials
    if(empty($username_err) && empty($password_err)) {
        // Prepare a select statement
        $sql = "SELECT user_id, username, password FROM usern WHERE username = :username";

        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":username", $param_username, PDO::PARAM_STR);
            // Set parameters
            $param_username = trim($_POST["username"]);

            // Attempt to execute the prepared statement
            if($stmt->execute()) {

                // Check if username exists, if yes then verify password
                if($stmt->rowCount() == 1) {
                    if($row = $stmt->fetch()) {
                        $id              = $row["user_id"];
                        $username        = $row["username"];
                        $hashed_password = $row["password"];
                        if(hash("sha256",$password) == $hashed_password){
                            // Password is correct, so start a new session
                            session_start();

                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"]       = $id;
                            $_SESSION["username"] = $username;

                            $date_time = date('Y-m-d H:i:s');
                            // Update login time in usern table
                            $sql = "UPDATE usern SET date_logged = :date_time WHERE user_id = :id";
                            if ($stmt = $pdo->prepare($sql)) {
                                $stmt->bindParam(":date_time",$date_time, PDO::PARAM_STR);
                                $stmt->bindParam(":id",       $id,        PDO::PARAM_INT);
                                $stmt->execute();
                            }

                            // Redirect user to welcome page
                            header("location: ../index.php?page=main_page");
                        } else {
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
                header("location: ../index.php?page=login_page");
            }
        }

        // Close statement
        unset($stmt);
    }

    // Close connection
    unset($pdo);
}
?>