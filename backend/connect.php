<?php
    function getDB() {
        $servername = 'localhost';
        $username = 'root';
        $password = '';
        $charset = 'utf8';
        $db_name = 'booktest';
        try {
            $connection = new PDO("mysql:host=" . $servername .";dbname=" . $db_name . ";charset=" . $charset, $username, $password);
            // set the PDO error mode to exception
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }