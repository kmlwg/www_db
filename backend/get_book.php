<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 31.12.18
 * Time: 12:18
 */

include_once "models/Book.php";
require_once "connect.php";

function getBook()
{
    $pdo = getDB();
    $book_err = "";


    if (!isset($_GET["bookID"])) {
        $book_err = 'Variable $_GET["bookID"] is not set';
    } elseif (empty($_GET["bookID"])) {
        $book_err = 'Variable $_GET["bookID"] is empty';
    } else {
        $bookID = htmlspecialchars($_GET["bookID"]);

        $sql = "SELECT book.title_pl, book.title_ori, author.author_id, author.firstname,
              author.surname, book.isbn, book.year_published, book.publisher, book.page_number 
              FROM ((author_book
                INNER JOIN book ON author_book.book_id = book.book_id)
                INNER JOIN author ON author_book.author_id = author.author_id)
              WHERE book.book_id = :bookID";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":bookID", $bookID, PDO::PARAM_INT);

            if ($stmt->execute()) {
                if ($stmt->rowCount() > 0) {
                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    $_SESSION["bookID"] = $bookID;
                    $book = new Book($result['title_pl'], $result["title_ori"], $result['author_id'], 
                                     $result['firstname'],$result['surname'], $result['isbn'], 
                                     $result['year_published'], $result['publisher'], $result['page_number']);
                    return $book;
                } else {
                    echo "No reslut";
                }

            } else {
                echo "Bad";
            }
        }
        unset($stmt);
    }
    unset($pdo);
}

function getTitle()
{
    $pdo = getDB();
    $book_err = " ";

    if (!isset($_GET['authorID'])) {
        $book_err = "authorID is not set";
    } elseif (empty($_GET['authorID'])) {
        $book_err = "bookID is empty";
    } else {
        $authorID = htmlspecialchars($_GET['authorID']);

        $sql = "SELECT book.title_pl, book.book_id
                FROM ((author_book 
                  INNER JOIN book ON author_book.book_id = book.book_id)
                  INNER JOIN author ON author_book.author_id=author.author_id)
                WHERE author.author_id = :authorID
                ORDER BY book.title_pl";
        if ($stmt=$pdo->prepare($sql)) {
            $stmt->bindParam(":authorID",$authorID,PDO::PARAM_INT);

            if($stmt->execute()) {
                if ($stmt->rowCount() >= 1) {
                    $titles = array();
                    $size = $stmt->rowCount();
                    for($i=0;$i<$size;$i++) {
                        $title = $stmt->fetch(PDO::FETCH_ASSOC);
                        $titles[] = new BookTitleHref($title['title_pl'],$title['book_id']);
                    }

                    return $titles;
                }
                else echo "rows <= 0";
            }
            else echo "STmt not exexcuted";
        }
        else echo "sql not prepared";

        unset($stmt);
    }
    echo $book_err;
    unset($pdo);
}

function getTopBooks() {
    $pdo = getDB();
    $result = array();

    $sql = "SELECT book.book_id, book.title_pl, book.rating, author.author_id, author.firstname, author.surname
            FROM ((author_book
              INNER JOIN book   ON author_book.book_id   = book.book_id)
              INNER JOIN author ON author_book.author_id = author.author_id)
            GROUP BY book.rating DESC
            LIMIT 100";
    
    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            $raw = $stmt->fetchAll();
            foreach($raw as &$record) {
                $result[] = new BookTop($record["book_id"], $record["title_pl"],
                                        $record["rating"], $record["author_id"],
                                        $record["firstname"], $record["surname"]);
            }
            return $result;
        } else {
            unset($stmt);
            unset($pdo);
        }
    } else {
        unset($stmt);
        unset($pdo);
    }
}

function getMyBooks() {
    $pdo = getDB();
    $result = array();

    $userID = 0;
    $userID_err = "";
    if (isset($_SESSION["id"])) {
        $userID = $_SESSION["id"];
    } else {
        $userID = "User is not set";
    }

    
    $sql = "SELECT book.book_id, book.title_pl, book.rating, author.author_id, author.firstname, author.surname
            FROM book 
              LEFT JOIN user_book ub ON book.book_id = ub.book_id
              LEFT JOIN usern ON ub.user_id = usern.user_id
              LEFT JOIN author_book ab ON book.book_id = ab.book_id
              LEFT JOIN author ON ab.author_id = author.author_id
            WHERE usern.user_id = :userID
            GROUP BY book.title_pl
            ";
    
    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $raw = $stmt->fetchAll();
            foreach($raw as &$record) {
                $result[] = new BookTop($record["book_id"], $record["title_pl"],
                                        $record["rating"], $record["author_id"],
                                        $record["firstname"], $record["surname"]);
            }
            return $result;
        } else {
            unset($stmt);
            unset($pdo);
        }
    } else {
        unset($stmt);
        unset($pdo);
    }
}

function getListBooks() {
    require_once "connect.php";
    $pdo = getDB();
    
    

    $sql = "SELECT book.book_id, book.title_pl, author.author_id, author.firstname, author.surname 
                FROM ((author_book
                  INNER JOIN book ON author_book.book_id = book.book_id)
                  INNER JOIN author ON author_book.author_id = author.author_id)";

    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            if($stmt->rowCount() >= 1) {
                $raw = $stmt->fetchAll();
                foreach($raw as &$result) {
                    $results[] = new A_BookSearchResult($result['book_id'],$result['title_pl'],
                                                        $result['firstname'],$result['surname']);
                }
                unset($stmt);
                unset($pdo);
                return $results;
            } else {
                echo "There are no results";
            }
        } else {
            echo "Something went wrong";
        }
    }

    unset($stmt);
    unset($pdo);

}