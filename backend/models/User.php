<?php



// Holds data for listing users
class UserSearchResult
{
    public $id;
    public $username;
    public $last_logged;
    public $url_delete;

    public function __construct($id, $username, $last_logged) {
        $this->id = $id;
        $this->username = $username;
        $this->last_logged = $last_logged;
        $this->url_delete = "index.php?page=deleteUser&userID=" . $id;
    }
}