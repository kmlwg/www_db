<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 05.01.19
 * Time: 16:53
 */

class Article
{
    public $title;
    public $content;
    public $created;
    public $author;

    /**
     * Article constructor.
     * @param $title
     * @param $content
     * @param $crested
     * @param $author
     */
    public function __construct($title, $content, $created, $author)
    {
        $this->title = $title;
        $this->content = $content;
        $this->created = $created;
        $this->author = $author;
    }
}

class ArticleHeader 
{
    public $title;
    public $content;
    public $created;
    public $author;
    public $link;


    /**
     * Article constructor.
     * @param $id
     * @param $title
     * @param $content
     * @param $crested
     * @param $author
     */
    public function __construct($id, $title, $content, $created, $author)
    {
        $this->title = $title;
        $this->content = $content;
        $this->created = $created;
        $this->author = $author;
        $this->link = "index.php?page=article&articleID=" . $id;
    }
}