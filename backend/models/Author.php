<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 30.12.18
 * Time: 22:57
 */

class Author
{
    public $name;
    public $surname;
    public $born;
    public $died;
    public $country;

    /**
     * Author constructor.
     * @param $name
     * @param $surname
     * @param $born
     * @param $died
     * @param $country
     */
    public function __construct($name, $surname, $born, $died, $country)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->born = $born;
        $this->died = $died;
        $this->country = $country;
    }
}

class AuthorSearchResult {
    public $id;
    public $name;
    public $surname;
    public $url_edit;
    public $url_delete;

    public function __construct($id, $name, $surname) {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->url_edit = "index.php?page=editAuthor&authorID=" . $id;
        $this->url_delete = "index.php?page=deleteAuthor&authorID=" . $id;
    }
}