<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 29.12.18
 * Time: 15:05
 */

class Result
{
    public $book_id;
    public $book_title;
    public $book_href;
    public $author_id;
    public $author_name;
    public $author_surname;
    public $author_href;

    /**
     * Result constructor.
     * @param $book_id
     * @param $book_title
     * @param $author_id
     * @param $author_name
     * @param $author_surname
     */
    public function __construct($book_id, $book_title, $author_id, $author_name, $author_surname)
    {
        $this->book_id = $book_id;
        $this->book_title = $book_title;
        $this->book_href = "index.php?page=book_page&bookID=" . $book_id;
        $this->author_id = $author_id;
        $this->author_name = $author_name;
        $this->author_surname = $author_surname;
        $this->author_href = "index.php?page=author_page&authorID=" . $author_id;
    }
}