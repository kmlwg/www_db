<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 30.12.18
 * Time: 22:45
 */

class Book
{
    public $title_pl;
    public $title_ori;
    public $author_name;
    public $author_surname;
    public $isbn;
    public $year;
    public $publisher;
    public $pages;
    public $author_href;

    /**
     * Book constructor.
     * @param $title_pl
     * @param $title_ori
     * @param $author_name
     * @param $author_surname
     * @param $isbn
     * @param $year
     * @param $publisher
     * @param $pages
     */
    public function __construct($title_pl, $title_ori, $author_id, $author_name, $author_surname, $isbn, $year,
                                $publisher, $pages)
    {
        $this->title_pl = $title_pl;
        $this->title_ori = $title_ori;
        $this->author_name = $author_name;
        $this->author_surname = $author_surname;
        $this->isbn = $isbn;
        $this->year = $year;
        $this->publisher = $publisher;
        $this->pages = $pages;
        $this->author_href = "index.php?page=author_page&authorID=" . $author_id;
    }


}

class BookTop
 {
    public $book_title;
    public $book_rating;
    public $book_href;
    public $author_name;
    public $author_surname;
    public $author_href;

    public function __construct($book_id, $book_title, $book_rating,
                                $author_id, $author_name, $author_surname)
    {
        $this->book_title = $book_title;
        $this->book_rating = $book_rating;
        $this->book_href = "index.php?page=book_page&bookID=" . $book_id;
        $this->author_name = $author_name;
        $this->author_surname = $author_surname;
        $this->author_href = "index.php?page=author_page&authorID=" . $author_id;
    }
 }

 class BookTitleHref
{
    public $title;
    public $href;

    /**
     * BookTitleHref constructor.
     * @param $title
     * @param $href
     */
    public function __construct($title, $bookID)
    {
        $this->title = $title;
        $this->href = "index.php?page=book_page&bookID=" . $bookID;
    }

}

class A_BookSearchResult {
    public $title;
    public $author_name;
    public $author_surname;
    public $url_edit;
    public $url_delete;

    public function __construct($id, $title, $author_name, $author_surname) {
        $this->title = $title;
        $this->author_name = $author_name;
        $this->author_surname = $author_surname;
        $this->url_edit = "index.php?page=editBook&bookID=" . $id;
        $this->url_delete = "index.php?page=deleteBook&bookID=" . $id;
    }
}