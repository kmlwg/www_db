<?php

class Comment
{
    public $content;
    public $author;
    public $created;

    public function __construct($content, $author, $created) {
        $this->content = $content;
        $this->author  = $author;
        $this->created = $created;
    }
}