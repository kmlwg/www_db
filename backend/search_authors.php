<?php
require_once "models/Author.php";
require_once "connect.php";
session_start();


$pdo = getDB();

$min_length = 3;
$query_error = "";

if (empty($_POST["query"])) {
    $query_error = "Query is empty";
} elseif (strlen(trim($_POST["query"])) < $min_length) {
    $query_error = "Query is less than 3 characters";
} else {
    $query = htmlspecialchars($_POST["query"]);
    $query = explode(" ", $query);
    $results = array();

    foreach ($query as &$item) {
        $sql = "SELECT a.author_id, a.firstname, a.surname
                FROM author a
                WHERE a.firstname LIKE '%{$item}%'
                   OR a.surname   LIKE '%{$item}%'";
    
        if ($stmt = $pdo->prepare($sql)) {
            //$stmt->bindParam(":item", $item, PDO::PARAM_STR);

            if ($stmt->execute()) {
                if ($stmt->rowCount() >= 1) {
                    $raw = $stmt->fetchAll();
                    foreach($raw as &$record) {
                        $results[] = new AuthorSearchResult($record["author_id"], $record["firstname"],
                                                            $record["surname"]);
                    }
                }
            }
        }
    }
    $results = array_unique($results, SORT_REGULAR);
    $_SESSION["results"] = $results;
    //var_dump($results);
    header("location: ../index.php?page=searchAuthor");
    unset($stmt);
}
unset($pdo);