<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 04.01.19
 * Time: 12:13
 */

//Connect to database
require_once "connect.php";
$pdo = getDB();

$titlePl = $titleOri = $isbn = $firstname = $surname = $publisher = $genre = "";
$year = $rating = $pages = $authorID = $bookID = 0;
$book_err = $author_error = $db_error = "";

$isbn = htmlspecialchars(trim($_POST["isbn"]));

$sql = "SELECT book.isbn FROM book WHERE book.isbn = :isbn";

if($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":isbn", $isbn, PDO::PARAM_STR);
    if ($stmt->execute()) {
        if ($stmt->rowCount() > 0) {
            $book_err = "Book exists";
            unset($stmt);
        }
    }
}

if (empty($book_err)) {
    $firstname = htmlspecialchars(trim($_POST["firstname"]));
    $surname   = htmlspecialchars(trim($_POST["surname"]));

    $sql = "SELECT author.author_id FROM author 
            WHERE author.firstname = :firstname AND 
                  author.surname = :surname";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
        $stmt->bindParam(":surname", $surname, PDO::PARAM_STR);

        if ($stmt->execute()) {
            if ($stmt->rowCount() < 1) {
                $author_err = "Author does not exist";
                header("location: ../index.php?page=add_author&author=1");
            } else {
                $raw = $stmt->fetch();
                $authorID = $raw["author_id"];
            }
        }
    }
}

if (empty($book_err) and empty($author_err)) {
    if(!isset($_POST["pages"])) {
        $_POST["pages"] = 0;
    }
    $sql = "INSERT INTO book (title_pl, title_ori, isbn, year_published, page_number, publisher, rating, genre) 
            VALUES (:titlePl, :titleOri, :isbn, :year, :pages, :publisher, :rating, :genre)";
    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":titlePl",   $titlePl,   PDO::PARAM_STR);
        $stmt->bindParam(":titleOri",  $titleOri,  PDO::PARAM_STR);
        $stmt->bindParam(":isbn",      $isbn,      PDO::PARAM_STR);
        $stmt->bindParam(":year",      $year,      PDO::PARAM_INT);
        $stmt->bindParam(":pages",     $pages,     PDO::PARAM_INT);
        $stmt->bindParam(":publisher", $publisher, PDO::PARAM_STR);
        $stmt->bindParam(":rating",    $rating,    PDO::PARAM_INT);
        $stmt->bindParam(":genre",     $genre,     PDO::PARAM_STR);

        $titlePl   = htmlspecialchars($_POST["titlePl"]);
        $titleOri  = htmlspecialchars($_POST["titleOri"]);
        $year      = htmlspecialchars($_POST["year"]);
        $publisher = htmlspecialchars($_POST["publisher"]);
        $pages     = htmlspecialchars($_POST["pages"]);
        $genre     = htmlspecialchars($_POST["genre"]);
        $rating    = htmlspecialchars($_POST["rating"]);


        if ($stmt->execute()) {
            // Get the ID of inserted book
            $sql = "SELECT book.book_id FROM book WHERE book.isbn = :isbn";

            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":isbn", $isbn, PDO::PARAM_STR);
                if ($stmt->execute()) {
                    $raw = $stmt->fetch();
                    $bookID = $raw["book_id"];
                    echo $bookID;
                }
            }
        } else {
            $db_error = "Failed to execute inserting book";
        }

    } else {
        $db_error = "Failed to bind prepare stmt";
    }
}




if (empty($db_error) and isset($_FILES["cover"])){
    $targetDir  = "../resources/img_book/";
    $uploadOk = false;
    $targetFile = $targetDir . basename($_FILES["cover"]["name"]);
    $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));
    $targetFile = $targetDir . $bookID . ".jpg";
    echo "BOOKID $bookID";


    $check = getimagesize($_FILES["cover"]["tmp_name"]);
    if ($check) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = true;
    } else {
        echo "File is not an image.";
        $uploadOk = false;
    }
    // No need to check if file exists since they're always renamed
    
    //Check file size
    if ($_FILES["cover"]["size"] > 500000) {
        echo "Sorry your file is to large.";
        $uploadOk = false;
    }

    //Allowed file formats
    if ($imageFileType != "jpg" and $imageFileType != "png" and $imageFileType != "jpeg") {
        echo "Sorry only jpg, jpeg, png files allowed";
        $uploadOk = false;
    }

    if ($uploadOk) {
        if (move_uploaded_file($_FILES["cover"]["tmp_name"], $targetFile)) {
            echo "The file ". basename( $_FILES["cover"]["name"]). " has been uploaded.";
        } else {
            echo "There was an error while uploading file.";
        }
    } else {
        echo "File was not uploaded";
    }

}

if ($bookID !== 0 && $authorID !== 0) {
    $sql = "INSERT INTO author_book (author_id, book_id) VALUES (:authorID, :bookID)";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":authorID", $authorID, PDO::PARAM_INT);
        $stmt->bindParam(":bookID",   $bookID,   PDO::PARAM_INT);

        if ($stmt->execute()) {
            //$link = "location: ../index.php?page=book_page&bookID=$bookID";
            //header($link);
        }
    }
}

unset($stmt);
unset($pdo);