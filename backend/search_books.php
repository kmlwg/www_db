<?php

session_start();

require_once "connect.php";
require_once "models/Book.php";

$pdo = getDB();

$query = htmlspecialchars($_POST["query"]);
$query = explode(" ", $query);

$results = array();

foreach ($query as $item) {
    $sql = "SELECT book.book_id, book.title_pl, author.author_id, author.firstname, author.surname 
        FROM ((author_book
          INNER JOIN book   ON author_book.book_id   = book.book_id)
          INNER JOIN author ON author_book.author_id = author.author_id)
        WHERE book.title_pl  LIKE '%{$item}%'
           OR book.title_ori LIKE '%{$item}%' 
           OR author.surname LIKE '%{$item}%'";
    
    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            if ($stmt->rowCount() >= 1) {
                $raw = $stmt->fetchAll();
                foreach ($raw as $record) {
                    $results[] = new A_BookSearchResult($result['book_id'],$result['title_pl'],
                                                        $result['firstname'],$result['surname']);
                }
            }
        }   
    }   
}

$results = array_unique($results, SORT_REGULAR);
$_SESSION["results"] = $results;
header("location: ../index.php?page=searchBook");

unset($stmt);
unset($pdo);
