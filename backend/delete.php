<?php

require_once "backend/connect.php";

// Delete user from database. Thanks to ON DELETE CASCADE, user's comments and articles 
// are deleted automatically as well as relations to books.
function deleteUser($userID) {
    $pdo = getDB();

    $sql = "DELETE FROM usern WHERE usern.user_id = :userID";

    if($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
        if ($stmt->execute()) {
           // header("location: index.php?page=users");
           unset($stmt);
           unset($pdo);
        }
    }
}

//Delte book from database. Automaticaly deletes relations to users and authors
function deleteBook($bookID) {
    $pdo = getDB();

    $sql = "DELETE FROM book WHERE book.book_id = :bookID";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":bookID", $bookID, PDO::PARAM_INT);
        if ($stmt->execute()) {
            //header("location: index.php?page=books");
            unset($stmt);
            unset($pdo);
        }
    }
}


function deleteAuthor($authorID) {
    $pdo = getDB();
    // First get all the id's of books related to author

    $sql = "SELECT b.book_id 
            FROM author_book ab
              INNER JOIN book b ON ab.book_id = b.book_id
              INNER JOIN author a ON ab.author_id = a.author_id
            WHERE a.author_id = :authorID";

    $bookRm_error = "";

    $books_to_rm = array();

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam("authorID", $authorID, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $books_to_rm = $stmt->fetchAll();
        } else {
            $bookRm_error = "Failed tp execute stmt";
        }
    } else {
        $bookRm_error = "Failed to prepare sql";
    }

    // Delete books
    if (empty($bookRm_error)) {
       foreach($books_to_rm as &$book) {
            deleteBook($book["book_id"]);
        }
    }

    // Relations between author and books were deleted automatically. 
    // Now delete author

    $sql = "DELETE FROM author WHERE author.author_id = :authorID";

    //if (empty($bookRm_error)) {
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":authorID", $authorID, PDO::PARAM_INT);
            if ($stmt->execute()) {
               // header("location: index.php?page=authors");
                unset($stmt);
                unset($pdo);
            }
        }
    //}
}

