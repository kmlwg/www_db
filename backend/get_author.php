<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 31.12.18
 * Time: 12:18
 */

include_once "models/Author.php";
require_once "connect.php";

function getAuthor()
{
    $pdo = getDB();

    $author_err = "";


    if (!isset($_GET["authorID"])) {
        $author_err = 'Variable $_GET["authorkID"] is not set';
    } elseif (empty($_GET["authorID"])) {
        $author_err = 'Variable $_GET["authorID"] is empty';
    } else {
        $authorID = htmlspecialchars($_GET["authorID"]);

        $sql = "SELECT * FROM author WHERE author.author_id = :authorID";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":authorID", $authorID, PDO::PARAM_INT);

            if ($stmt->execute()) {
                if ($stmt->rowCount() > 0) {
                    $result = $stmt->fetch(PDO::FETCH_ASSOC);
                    $author = new Author($result['firstname'], $result["surname"], $result['date_born'],
                                         $result['date_death'], $result['country']);
                    
                    unset($stmt);
                    unset($pdo);
                    return $author;
                } else {
                    echo "No reslut";
                    unset($stmt);
                    unset($pdo);
                    return false;
                }

            } else {
                echo "Bad";
                unset($stmt);
                unset($pdo);
                return false;
            }
        }
        unset($stmt);
        unset($stmt);
        return false;
    }
}

function getListAuthors() {
    $pdo = getDB();

    $sql = "SELECT   author.author_id, author.firstname, author.surname 
            FROM     author
            GROUP BY author.surname";

    $result = array();


    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            $raw = $stmt->fetchAll();
            foreach ($raw as &$record) {
                $result[] = new AuthorSearchResult($record["author_id"], $record["firstname"], $record["surname"]);
            }
            unset($stmt);
            unset($pdo);
            return $result;
        } else {
            unset($stmt);
            unset($pdo);
            return null;
        }
    } else {
        unset($pdo);
        return null;
    }
}