<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 05.01.19
 * Time: 12:32
 */

require_once "connect.php";
$pdo = getDB();
session_start();

$userID = $_SESSION["id"];
$username = $_SESSION["username"];


$currentDate = date('Y-m-d H:i:s');

$title = $content = "";
$title_err = $text_err = "";
if (isset($_POST["fTitle"])) {
    if (!empty($_POST["fTitle"])) {
        $title = htmlspecialchars($_POST["fTitle"]);
    } else {
        $title_err = "Title is empty";
    }
} else {
    $tile_err = "Title is not set";
}

if (isset($_POST["fText"])) {
    if (!empty($_POST["fText"])){
        $content = htmlspecialchars($_POST["fText"]);
    } else {
        $text_err = "Text is empty";
    }
} else {
    $text_err = "Text is not set";
}


if (empty($title_err) && empty($text_err)) {
    $sql = "INSERT INTO articles (title, content, date_created, user_id) 
            VALUES (:title, :content, :currentDate, :userID)";
    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":title",       $title,       PDO::PARAM_STR);
        $stmt->bindParam(":content",     $content,     PDO::PARAM_STR);
        $stmt->bindParam(":currentDate", $currentDate, PDO::PARAM_STR);
        $stmt->bindParam(":userID",      $userID,      PDO::PARAM_INT);
        // print_r($currentDate);
        // echo "<br>";
        // print_r($title);
        // echo "<br>";
        // print_r($content);
        // echo "<br>";
        // print_r($userID);
        if($stmt->execute()) {
            //redirect to created article, to do so get ID from databse
            $sql = "SELECT article_id FROM articles WHERE user_id = :userID AND date_created = :currentDate";
            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":userID",      $userID,      PDO::PARAM_INT);
                $stmt->bindParam(":currentDate", $currentDate, PDO::PARAM_STR);
                if ($stmt->execute()) {
                    $raw = $stmt->fetch();
                    $articleID = $raw["article_id"];
                    header("location: ../index.php?page=article&articleID=$articleID");
                }
            }

        } else {
            // redirect somwhere else
        }
    }
}