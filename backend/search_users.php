<?php
require_once "models/User.php";
require_once "connect.php";
session_start();
$pdo = getDB();

$min_length = 3;
$query_error = "";

if (empty($_POST["query"])) {
    $query_error = "The query is empty";
} elseif (strlen(trim($_POST["query"])) < $min_length) {
    $query_error = "Query is less than 3 characters";
} else {
    $query = htmlspecialchars($_POST["query"]);
    $query = explode(" ", $query);
    $results = array();

    foreach ($query as &$item) {
        $sql = "SELECT u.user_id, u.username, u.date_logged
                FROM usern u
                WHERE u.username LIKE '%{$item}%'";

        if ($stmt = $pdo->prepare($sql)) {
            //$stmt->bindParam(":item", $item, PDO::PARAM_STR);

            if ($stmt->execute()) {
                if ($stmt->rowCount() >= 1) {
                    $raw = $stmt->fetchAll();
                    foreach($raw as &$record) {
                        $results[] = new UserSearchResult($record["user_id"], $record["username"], 
                                                          $record["date_logged"]);
                    } 
                }
            }
        }
    }

    $results = array_unique($results, SORT_REGULAR);
    $_SESSION["results"] = $results;
    header("location: ../index.php?page=searchUser");
    usnet($stmt);
}
unset($pdo);
