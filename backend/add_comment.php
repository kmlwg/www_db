<?php 

require_once "connect.php";
session_start();
$pdo = getDB();

$userID = $_SESSION["id"];
$bookID = $_SESSION["bookID"];

$currentDate = date("Y-m-d H:m:s");

$content = $_POST["comment"];

$sql = "INSERT INTO comment (content, date_created, user_id, book_id) 
        VALUES (:content, :currentDate, :userID, :bookID)";

if ($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":content", $content, PDO::PARAM_STR);
    $stmt->bindParam(":currentDate", $currentDate, PDO::PARAM_STR);
    $stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
    $stmt->bindParam(":bookID", $bookID, PDO::PARAM_INT);

    if ($stmt->execute()) {
        header("location: ../index.php?page=book_page&bookID=$bookID");
    } else {
        header("location: ../index.php?page=book_page&bookID=$bookID");
    }
}

unset($pdo);
unset($stmt);