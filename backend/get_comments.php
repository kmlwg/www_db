<?php
require_once "connect.php";
require_once "models/Comment.php";



function getComments() {
    $pdo = getDB();

    if (isset($_SESSION["bookID"])) {
        $bookID = $_SESSION["bookID"];
    } elseif (isset($_GET["bookID"])) {
        $bookID = $_GET["bookID"];
    }


    $sql = "SELECT u.username, c.content, c.date_created
            FROM comment c
              LEFT JOIN usern u ON u.user_id = c.user_id
              LEFT JOIN book  b ON b.book_id = c.book_id
            WHERE b.book_id = :bookID";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":bookID", $bookID, PDO::PARAM_INT);
        

        if ($stmt->execute()) {
            $result = array();

            $raw = $stmt->fetchALl();
            foreach ($raw as &$record) {
                $result[] = new Comment($record["content"], $record["username"],
                                        $record["date_created"]);
            }
            return $result;
        }
    } else {
        echo "Couldnt prepare stmt";
        var_dump($stmt);
    }
            
}