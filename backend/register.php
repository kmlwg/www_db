<?php
/**
 * Created by PhpStorm.
 * User: kwgie
 * Date: 2018-12-12
 * Time: 15:49
 */
require_once "connect.php";
//session_start();
$pdo = getDB();

$username = $password = $confirm_password = $email = "";
$username_err = $password_err = $confirm_password_err = $email_error = "";


// Process form data when form is submitted
//if (isset($_POST["username"])) {
    // Validate username
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter a username.";
    } else {
        // Prepare a select statement
        $sql = "SELECT user_id FROM usern WHERE username = :username";

        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables to prepared statement as parameters
            $stmt->bindParam(":username", $param_username, PDO::PARAM_STR);

            //Set parameters
            $param_username = trim($_POST["username"]);

            // Attempt to execute the prepared statement
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    $username_err = "This username is already taken.";
                } else {
                    $username = trim($_POST["username"]);
                }
            } else {
                echo "Something went wrong. Please try again later.";
            }
        }
        // Close statement
        unset($stmt);
    }

    // Validate passwords
    if (empty(trim($_POST["pswd"]))) {
        $password_err = "Please enter password.";
    } elseif (strlen(trim($_POST["pswd"])) < 6){
        $password_err = "Password must have at least 6 characters.";
    } else {
        $password = trim($_POST["pswd"]);
    }

    // Validate confirm password
    if (empty(trim($_POST["pswd-r"]))) {
        $confirm_password_error = "Please confirm password.";
    } else {
        $confirm_password = trim($_POST["pswd-r"]);
        if(empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }

    // Validate email
    if (empty(trim($_POST["email"]))) {
        $email_error = "Please enter email";
    } else {
        // Prepare SELECT statement
        $sql = "SELECT user_id FROM usern WHERE email = :email";

        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables to prepared statement as parameters
            $stmt->bindParam(":email", $param_email, PDO::PARAM_STR);

            $param_email = trim($_POST["email"]);

            // Attempt to execute the statement
            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    $email_error = "This email is already taken";
                } else {
                    $email = trim($_POST["email"]);
                }
            } else {
                echo "Something went wrong try again later.";
            }
        }
        unset($stmt);
    }

    // Get current datetime in MySql format
    $date_created = date('Y-m-d H:i:s');

    // Check for input errors
    if (empty($username_err) && ($password_err != $confirm_password) && empty($confirm_password_err)) {
        // Prepare insert statement
        $sql = "INSERT INTO usern (username, password, email, date_created) 
                           VALUES (:username, :password, :email, :date_created)";

        if ($stmt = $pdo->prepare($sql)) {
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":username",     $param_username, PDO::PARAM_STR);
            $stmt->bindParam(":password",     $param_password, PDO::PARAM_STR);
            $stmt->bindParam(":email",        $param_email,    PDO::PARAM_STR);
            $stmt->bindParam(":date_created", $param_date,     PDO::PARAM_STR);


            // Set parameters
            $param_username = $username;
            $param_password = hash("sha256", $password);    // hash password
            $param_email = $email;
            $param_date = $date_created;

            // Attempt to execute prepared statement
            if ($stmt->execute()) {
                header("location: ../index.php?page=login_page");
            } else {
                echo "Something went wrong. Please try again.";
                header("location: ../index.php?page=register_page");
            }
        }
        unset($stmt);
    }
    unset($pdo);
//}
