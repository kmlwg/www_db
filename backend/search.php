<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 18.12.18
 * Time: 19:57
 */

include "models/Result.php";
session_start();

//require_once "include/PHPTAL.php";
require_once "connect.php";
$pdo = getDB();


$min_length = 3;

$query_error = "";

//TODO Add advanced search: choose authors or books

if(empty($_POST["query"])) {
    $query_error = "Query is empty";
} elseif (strlen(trim($_POST["query"])) <= $min_length) {
    $query_error = "Query is to short";
} else {
    $query = htmlspecialchars($_POST["query"]);
    $query = explode(" ",$query);
    $results = array();

    foreach ($query as $item) {
        $sql = "SELECT book.book_id, book.title_pl, author.author_id, author.firstname, author.surname 
                  FROM ((author_book
                    INNER JOIN book ON author_book.book_id = book.book_id)
                    INNER JOIN author ON author_book.author_id = author.author_id)
                  WHERE book.title_pl LIKE '%{$item}%'
                     OR book.title_ori LIKE '%{$item}%' 
                     OR author.surname LIKE '%{$item}%'";

        if ($stmt = $pdo->prepare($sql)) {
            if ($stmt->execute()) {
                if($stmt->rowCount() >= 1) {
                    $size = $stmt->rowCount();
                    for($i=0; $i<$size;$i++) {
                        $result = $stmt->fetch(PDO::FETCH_ASSOC);
                        $results[] = new Result($result['book_id'],$result['title_pl'],
                                                        $result['author_id'],$result['firstname'],$result['surname']);
                    }
                } else {
                    echo "There are no results";
                }
            } else {
                echo "Something went wrong";
            }
        }
    }

    // Check for duplicates, if necesary delete them
    $results = array_unique($results, SORT_REGULAR);
    $_SESSION["results"] = $results;
    header("location: ../index.php?page=search_page_result");
    unset($stmt);
}
unset($pdo);