<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 01.01.19
 * Time: 13:41
 */

// Connect to database
require_once "connect.php";
$pdo = getDB();

// Init variables
$firstname = $surname = $country = "";
$born = $died = "";

$errors = array();
$warnings = array();
$firstname = htmlspecialchars(trim($_POST["firstname"]));
$surname   = htmlspecialchars(trim($_POST["surname"]));

if (isset($_POST["country"])) {
    if(!empty($_POST["country"])) {
        $country = htmlspecialchars(trim($_POST["country"]));
    } else {
        $warnings[] = 'WARINING: $_POST["country"] is empty';
        $country = 'n.a';
    }
} else {
    $warnings[] = 'WARINING: $_POST["country"] is not set';
    $country = 'n.a';
}

$born = htmlspecialchars($_POST["date_born"]);

if (isset($_POST["date_death"])) {
    if(is_int($_POST["date_death"])) {
        $died = htmlspecialchars($_POST["date_death"]);
    } else {
        $warnings[] = 'WARINING: $_POST["date_death"] is not int';
        $died = 0;
    }
} else {
    $warnings[] = 'WARINING: $_POST["date_death"] is not set';
    $died = 0;
}

$toInsert = false;

$sql = "SELECT author.author_id FROM author
          WHERE author.firstname = :firstname AND
                author.surname   = :surname   AND 
                author.date_born = :born";

if ($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
    $stmt->bindParam(":surname",   $surname,   PDO::PARAM_STR);
    $stmt->bindParam(":born",      $born,      PDO::PARAM_INT);

    if ($stmt->execute()) {
        if ($stmt->rowCount() == 1) {
            $errors[] = "ERROR: author already exists";
        } else {
            $toInsert = true;
            unset($stmt);
        }
    }
} else {
    unset($stmt);
    $errors[] = "ERROR: Failed to prepae sql";
}

if ($toInsert) {
    $sql = "INSERT INTO author (firstname, surname, date_born, date_death, country) 
            VALUES (:firstname, :surname, :born, :died, :country)";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
        $stmt->bindParam(":surname",   $surname,   PDO::PARAM_STR);
        $stmt->bindParam(":born",      $born,      PDO::PARAM_INT);
        $stmt->bindParam(":died",      $died,      PDO::PARAM_INT);
        $stmt->bindParam(":country",   $country,   PDO::PARAM_STR);

        if($stmt->execute()) {
            // Get ID of inserted actor, in order to redirect to it's rescpectfull author_page
            $sql = "SELECT author.author_id FROM author WHERE author.surname = :surname AND author.firstname = :firstname";
            if($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
                $stmt->bindParam(":surname",   $surname,   PDO::PARAM_STR);

                if($stmt->execute()) {
                    $raw = $stmt->fetch();
                    $authorID = $raw['author_id'];
                    header("location: ../index.php?page=author_page&authorID=$authorID");
                }
            }
        } else {
            $erros[] = "Smth went wrong";
            header("location: ../index.php?page=add_author");
        }
    }
    unset($stmt);
}
unset($pdo);