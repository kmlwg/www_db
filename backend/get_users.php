<?php
require_once "connect.php";
require_once "models/User.php";
function getListUsers() {
    

    $pdo = getDB();

    $sql = "SELECT u.user_id, u.username, u.date_logged FROM usern u";
    $result = array();


    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            if ($stmt->rowCount() >= 1) {
                $raw = $stmt->fetchAll();
                foreach ($raw as &$record) {
                    $result[] = new UserSearchResult($record['user_id'], $record['username'], $record["date_logged"]);
                }
                unset($stmt);   
                unset($pdo);
                return $result;
            } else {
                echo "No results";
                unset($stmt);
                unset($pdo);
                return null;
            }
        } else {
            echo "Failed to execute $sql";
            unset($pdo);
            return null;
        }
    } else {
        echo "Failed to prepare SQL";
        usnet($pdo);
        return(null);
    }
}