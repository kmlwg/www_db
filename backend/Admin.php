<?php

class Admin {
    public static function check() {
        session_start();

        if (isset($_GET['logout'])) {    // Wylogowanie
            unset($_SESSION['admin']);
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: index.php');
            die();	
        }

        require_once "connect.php";
        $pdo = getDB();
        $sql = "SELECT password FROM usern u WHERE u.username = :admin AND u.user_id = :adminID";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":admin", $admin, PDO::PARAM_STR);
            $stmt->bindParam(":adminID", $adminID, PDO::PARAM_INT);

            $admin = "admin";
            $adminID = 1;
        
            if ($stmt->execute()) {
                $raw = $stmt->fetch();
                $admin_pswd = $raw["password"];
                $password = hash("sha256", $_POST["pswd"]);

                if ($password = $admin_pswd) {
                    $_SESSION["admin"] = true;
                    
                    session_regenerate_id();
                    return true;
                }
            }
        }
    }
}