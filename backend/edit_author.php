<?php

require_once "connect.php";
session_start();

$pdo = getDB();

$sql = "UPDATE author
        SET firstname = :firstname, surname = :surname, date_born = :born, 
            date_death = :died, country = :country
        WHERE author_id = :authorID"; 
            

if ($stmt = $pdo->prepare($sql)) {
    $stmt->bindParam(":firstname", $firstname, PDO::PARAM_STR);
    $stmt->bindParam(":surname",   $surname,   PDO::PARAM_STR);
    $stmt->bindParam(":born",      $born,      PDO::PARAM_INT);
    $stmt->bindParam(":died",      $died,      PDO::PARAM_INT);
    $stmt->bindParam(":country",   $country,   PDO::PARAM_STR);
    $stmt->bindParam(":authorID",  $authorID,  PDO::PARAM_INT);

    $firstname = htmlspecialchars(trim($_POST["firstname"]));
    $surname   = htmlspecialchars(trim($_POST["surname"]));
    $born      = htmlspecialchars($_POST["date_born"]);
    $died      = htmlspecialchars($_POST["date_death"]);
    $country   = htmlspecialchars(trim($_POST["country"]));
    $authorID  = $_SESSION["authorID"];

    if($stmt->execute()) {
        header("location: ../index.php?page=editAuthor&authorID=$authorID");
    } else {
        $erros[] = "Smth went wrong";
        header("location: ../index.php?page=add_author");
    }
}
unset($stmt);

unset($pdo);