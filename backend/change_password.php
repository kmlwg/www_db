<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 31.12.18
 * Time: 22:27
 */

session_start();
require_once "connect.php";
$pdo=getDB();

if(!isset($_SESSION['id'])) {
    echo "User id is not set";
}
if(empty($_SESSION['id'])) {
    echo "User is empty";
}

$userID = $_SESSION['id'];
$pswd_err = $info = '';

if(empty(trim($_POST['pswd']))) {
    $pswd_err = "Empty password";
} else {
    $sql = "UPDATE usern SET password= :pswd WHERE usern.user_id = :userID";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":pswd"  , $param_pswd,  PDO::PARAM_STR);
        $stmt->bindParam(":userID", $param_userID,PDO::PARAM_INT);

        $param_pswd = hash("sha256", $_POST["pswd"]);
        $param_userID = $userID;
        if ($stmt->execute()) {
            $info = "Successfuly changed password";
            echo $pswd_err;
            unset($stmt);
            unset($pdo);
            header("location: ../index.php?page=main_page");
        } else {
            //echo $pswd_err;
            $pswd_err = 'Failed to execute $stmt';
            header("location: ../index.php?page=change-pswd");
        }
    } else {
        unset($pdo);
        $pswd_err = "Failed to prepare pdo";
    }
    unset($stmt);
}
echo $pswd_err;
unset($pdo);