<?php
/**
 * User: neri
 * Date: 6.01.2019
 * Time: 13:21
 */

require_once "connect.php";

session_start();
$pdo = getDB();

$userID = $bookID = 0;
$userID_err = $bookID_err = $realtion_err = $exec_err = "";

if (isset($_SESSION["id"])) {
   $userID = $_SESSION["id"];
} else {
    $userID_err = "User ID is not set";
}

if (isset($_SESSION["bookID"])) {
    $bookID = $_SESSION["bookID"];
} else {
    $bookID_err = "Book ID is not set";
}

// If IDs are set check if relationship between user and book already exists
if (empty($userID_err) && empty($bookID_err)) {
    $sql = "SELECT * FROM user_book WHERE user_book.user_id = :userID AND
                                          user_book.book_id = :bookID";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
        $stmt->bindParam(":bookID", $bookID, PDO::PARAM_INT);

        if ($stmt->execute()) {
            if ($stmt->rowCount() > 0) {
                $realtion_err = "Relationship alredy exists";
                // Redirect to the same book page with a warning
                header("location: ../index.php?page=book_page&bookID=$bookID&mes=1");
            }
        }
    }
}

// If no errors occured proceed if insertion do data base
if (empty($userID_err) && empty($bookID_err) && empty($realtion_err)) {
    $sql = "INSERT INTO user_book (user_book.user_id, user_book.book_id) VALUES (:userID, :bookID)";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
        $stmt->bindParam(":bookID", $bookID, PDO::PARAM_INT);

        if($stmt->execute()) {
            header("location: ../index.php?page=book_page&bookID=$bookID&mes=2");
        }
    }
}
// print_r($userID);
// echo "<br/>";
// print_r($userID_err);
// echo "<br/>";
// print_r($bookID);
// echo "<br/>";
// print_r($bookID_err);
// echo "<br/>";
// print_r($realtion_err);


unset($stmt);
unset($pdo);
    

