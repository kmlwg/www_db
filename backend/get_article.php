<?php
/**
 * Created by PhpStorm.
 * User: neri
 * Date: 05.01.19
 * Time: 16:53
 */

function getArticle()
{
    require_once "connect.php";
    require_once "models/Article.php";
    $pdo = getDB();

    $articleID = $_GET["articleID"];

    $sql = "SELECT articles.title, articles.content, articles.date_created, usern.username 
            FROM articles 
              INNER JOIN usern ON articles.user_id = usern.user_id
            WHERE articles.article_id = :articleID";

    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":articleID", $articleID, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $raw = $stmt->fetch(PDO::FETCH_BOTH);

            $result = new Article($raw[0], $raw[1], $raw[2], $raw[3]);
            unset($stmt);
            unset($pdo);
            return $result;
        } else {
            unset($stmt);
            unset($pdo);
            return new Article("boom","boom","boom","boom");
        }

    } else {
        unset($stmt);
        unset($pdo);
        return new Article("boom","boom","boom","boom");
    }
}

function getLatestArticles() {
    require_once "connect.php";
    require_once "models/Article.php";

    $pdo = getDB();

    $sql = "SELECT a.article_id, a.title, LEFT(a.content,500) AS content, a.date_created, u.username
            FROM articles a
              INNER JOIN usern u ON a.user_id = u.user_id
            GROUP BY a.date_created";

    $result = array();

    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            $raw = $stmt->fetchAll();
            foreach($raw as &$record) {
                $result[] = new ArticleHeader($record["article_id"], $record["title"],
                                              $record["content"], $record["date_created"],
                                              $record["username"]);
            }
            unset($pdo);
            unset($stmt);
            return $result;
        } else {
            echo "Failed execute stmt";
        }
    } else {
        echo "Faile to prepare stmt";
    }
}