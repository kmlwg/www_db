// Register form validation
function validateUsernameRegister() {
    "use strict";
    let x = window.document.forms["register"]["username"].value.trim();
    if (x.length <= 3) {
        window.alert("Nazwa użytkownika musi być dłuższa niż 3 znaki");
        return false;
    }
    return true;
}

function validatePasswordRegister() {
    "use strict";
    let password   = window.document.forms["register"]["pswd"].value.trim();
    let password_r = window.document.forms["register"]["pswd-r"].value.trim();
    if (password.length <= 5) {
        window.alert("Hasło musi być dłuższe niż 5 znaków");
        return false;
    }
    if (password_r.localeCompare(password)) {
        window.alert("Wprowadzone hasła nie są identyczne");
        return false;
    }
    return true;
}

function validateRegister() {
    "use strict";
    return (validateUsernameRegister() && validatePasswordRegister());
}

// Password chane form validation
function validatePasswordChange() {
    "use strict";
    let password   = window.document.forms["change-pswd"]["pswd"].value.trim();
    let password_r = window.document.forms["change-pswd"]["pswd-r"].value.trim()    ;
    if (password.length <= 5) {
        window.alert("Hasło musi być dłuższeniż 5 znaków");
        return false;
    }
    if (password_r.localeCompare(password)) {
        window.alert("Wprowadzone hasła nie są identyczne");
        return false;
    }
    return true;
}

// Add author form validation
function validateAddAuthor() {
    "use strict";
    let specialChars = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;

    let firstname = window.document.forms["add-author"]["firstnme"].value;
    if (firstname.trim().length < 2) {
        window.alert("Imię autora musi być dłuższe niż jedna litera");
        return false;
    } else if (specialChars.test(firstname)) {
        window.alert("Imię autora zawiera znaki specjalne. Dozwolone są jedynie litery");
        return false;
    } else if (/\d/.test(firstname)) {
        window.alert("Imię autora zawiera cyfry. Dozwolone są jedynie litery");
        return false;
    }

    let surname = window.document.forms["add-author"]["surname"].value;
    if (surname.trim().length < 2) {
        window.alert("Nazwisko autora musi być dłuższe niż jedna litera");
        return false;
    } else if (specialChars.test(surname)) {
        window.alert("Nazwisko autora zawiera znaki specjalne. Dozwolone są jedynie litery");
        return false;
    } else if (/\d/.test(surname)) {
        window.alert("Nazwisko autora zawiera znaki specjalne. Dozwolone są jedynie liczby");
        return false;
    }

    let dateBorn = window.document.forms["add-author"]["date-born"].value;
    let currentDate = new Date();
    if (!Number.isInteger(dateBorn)) {
        window.alert("Rok urodzenia musi być liczbą całkowitą");
        return false;
    } else if (dateBorn >= currentDate.getFullYear()) {
        window.alert("Nieprawidłowy rok urodzenia.");
        return false;
    }
    let dateDeath = window.document.forms["add-author"]["date-death"].value;
    if (dateDeath.toString().length !== 0) {
        if (!Number.isInteger(dateDeath)) {
            window.alert("Rok śmierci musi być liczbą całkowitą");
            return false;
        } else if (dateDeath > currentDate.getFullYear()) {
            window.alert("Nieprawidłowy rok śmierci.");
            return false;
        }
    }

    let country = window.document.forms["add-author"]["country"].value;
    if (country.trim().length !== 0) {
        if (country.trim().length < 2) {
            window.alert("Nazwisko autora musi być dłuższe niż jedna litera");
            return false;
        } else if (specialChars.test(country)) {
            window.alert("Nazwa kraju zawiera znaki specjalne. Dozwolone są jedynie litery");
            return false;
        } else if (/\d/.test(country)) {
            window.alert("Nazwa kraju zawiera znaki specjalne. Dozwolone są jedynie liczby");
            return false;
        }
    }
    return true;
}

// Add book form validation
function validateAddBook() {
    "use strict";
    let specialChars = /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;

    let titlePl  = window.document.forms["add-book"]["titlePl"].value;
    let titleOri = window.document.forms["add-book"]["titleOri"].value;
    if (titlePl.trim().length < 3 || titleOri.trim() < 3) {
        window.alert("Tytuł książki musi być dłuższy niż 3 znaki");
        return false;
    }

    let isbn = window.document.forms["add-Book"]["isbn"].value;
    if (!/\d/.test(isbn)) {
        window.alert("Numer ISBN zawiera niedozwolone znaki.");
        return false;
    } else if (isbn.length !== 13) {
        window.alert("Numer ISBN zwiera niedozwoloną ilość znaków");
        return false;
    }

    let firstname = window.document.forms["add-book"]["firstnme"].value;
    if (firstname.trim().length < 2) {
        window.alert("Imię autora musi być dłuższe niż jedna litera");
        return false;
    } else if (specialChars.test(firstname)) {
        window.alert("Imię autora zawiera znaki specjalne. Dozwolone są jedynie litery");
        return false;
    } else if (/\d/.test(firstname)) {
        window.alert("Imię autora zawiera cyfry. Dozwolone są jedynie litery");
        return false;
    }

    let surname = window.document.forms["add-book"]["surname"].value;
    if (surname.trim().length < 2) {
        window.alert("Nazwisko autora musi być dłuższe niż jedna litera");
        return false;
    } else if (specialChars.test(surname)) {
        window.alert("Nazwisko autora zawiera znaki specjalne. Dozwolone są jedynie litery");
        return false;
    } else if (/\d/.test(surname)) {
        window.alert("Nazwisko autora zawiera znaki specjalne. Dozwolone są jedynie liczby");
        return false;
    }

    let year = window.document.forms["add-book"]["year"].value;
    let currentDate = new Date();

    if (!Number.isInteger(year)) {
        window.alert("Rok musi być liczbą całkowitą");
        return false;
    } else if (year > currentDate.getFullYear() || year === 0) {
        window.alert("Błędna wartość roku wydania książki");
        return false;
    }

    let publisher = window.document.forms["add-book"]["publisher"].value;
    if (publisher.trim().length < 3) {
        window.alert("Nazwa wydawnictwa musi mieć minimalnie 3 znaki");
        return false;
    } else if (specialChars.test(publisher)) {
        window.alert(("Nazwa wydawnictwa zawiera niedozwolone znaki"));
        return false;
    }

    return true;
}
