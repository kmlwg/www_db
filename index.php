<?php
/**
 * Created by PhpStorm.
 * User: kwgie
 * Date: 2018-12-12
 * Time: 14:48
 */

// parsing adress
$page = isset($_GET['page']) ?  $_GET['page'] : 'main_page';

include "include/PHPTAL.php";
include "backend/models/Result.php";
include "backend/models/Book.php";
include "backend/models/Article.php";
include "backend/models/Author.php";
include "backend/models/User.php";
include "backend/get_book.php";
include "backend/get_author.php";
include "backend/get_article.php";
include "backend/get_comments.php";
include "backend/get_users.php";
include "backend/delete.php";


session_start();

// Initialize the loggedin variable and check if user is logged
$loggedin = false;
if (isset($_SESSION["loggedin"]) and $_SESSION["loggedin"]) {
    $loggedin = true;
}

// Create new template
$template = new PHPTAL();
// Store information whether user is logged in template
$template->logged = $loggedin;

$admin = false;
if (isset($_SESSION["admin"]) and $_SESSION["admin"]) {
    $admin = $_SESSION["admin"];
}
$template->admin = $admin;

try {
    $template->setOutputMode(PHPTAL::HTML5);
} catch (PHPTAL_ConfigurationException $e) {
    echo $e;
}

// Prevent accesing unwanted pages and set the template
if (!$loggedin) {
    switch ($page) {
        case 'main_page':
        case 'top_page':
        case 'about_page':
        case 'search_page':
        case 'login_page':
        case 'register_page':
        case 'book_page':
        case 'author_page':
        case 'search_page_result':
        case 'article':
        case 'admin_login':
            $template->setTemplate("frontend/$page.html");
            break;
        case 'admin':
            include "backend/Admin.php";
            $logAdmin = Admin::check();
            if ($logAdmin) header("location: index.php?page=books");
            break;
        default:
            $template->setTemplate("frontend/main_page.html");
            break;
    }
} elseif ($loggedin) {
    switch ($page) {
        case 'add_author':
        case 'add_book':
        case 'add_article':
        case 'my_books':
        case 'change_pswd':
        case 'main_page':
        case 'top_page':
        case 'about_page':
        case 'search_page':
        case 'login_page':
        case 'register_page':
        case 'book_page':
        case 'author_page':
        case 'search_page_result':
        case 'article':
            $template->setTemplate("frontend/$page.html");
        break;
    default:
        $template->setTemplate("frontend/main_page.html");
        break;

    }
}





// Transfer data to to it's respected template
switch ($page) {
    case 'main_page':
        $template->articles = getLatestArticles();
        break;
    case 'top_page':
        $template->result = getTopBooks();
    case 'about_page':
    case 'search_page':
    case 'login_page':
    case 'register_page':
    case 'book_page':        
        // Get the path to image
        if (isset($_SESSION["bookID"])) {
            $cover = "resources/img_book/" . $_SESSION["bookID"] . ".jpg";
            $template->cover = $cover;
        } elseif (isset($_GET["bookID"])) {
            $cover = "resources/img_book/" . $_GET["bookID"] . ".jpg";
            $template->cover = $cover;
        }
        // Get the array of comments if exists
        $comments = getComments();
        $commentsExist = false;
        if (isset($comments)) {
            $template->comments = $comments;
            $commentsExist = true;
            $template->commentsExist = $commentsExist;
        }
        
        $template->book = getBook();
        $template->mes = 0;
        if (isset($_GET["mes"]) and !empty($_GET["mes"])) {
            $template->mes = $_GET["mes"];
        }
        break;
    case 'author_page':
        $template->author = getAuthor();
        $template->titles = getTitle();
        break;
    case 'search_page_result':
        if (isset($_SESSION["results"])) {
            $template->results = $_SESSION["results"];
        } else {
        echo '$_Session not set';
        }
        break;
    case 'article':
        $template->Article = getArticle();
        break;
    default:
        break;
    
}

if ($loggedin) {
    switch ($page) {
        case 'add_author':
            $template->authorExist = false;
            if (isset($_GET['author']) and !empty($_GET['author'])) {
                $template->authorExist = $_GET['author'];
            }
            break;
        case 'add_book':
        case 'add_article':
        case 'my_books':
            $template->result = getMyBooks();
            break;
        case 'change_pswd':
    }
}

// Flow of amdin panel
if (isset($_SESSION["admin"]) and $_SESSION["admin"]) {
    $adminDir = "frontend/admin/";
    switch($page) {
        case 'books':
        case 'authors':
        case 'comments':
        case 'users':
        case 'editBook':
        case 'editAuthor':
            $template->setTemplate($adminDir . $page . ".html");
            break;
        case 'deleteBook':
        case 'deleteAuthor':
        case 'deleteUser':
        case 'searchAuthor':
            break;
        default:
            $template->setTemplate($adminDir . "books.html");
            break;
    }
}


// Customizing templates
if (isset($_SESSION["admin"]) and $_SESSION["admin"]) {
    $adminDir = "frontend/admin/";

    switch($page) {
        case 'deleteBook':
            if (isset($_GET["bookID"])) {
                deleteBook($_GET["bookID"]);
            }
            $template->setTemplate($adminDir . "books.html");
            $template->results = getListBooks();
            break;
        case 'editBook':
            if (isset($_GET["bookID"])) {
                $template->book = getBook();
                $_SESSION["bookID"] = $_GET["bookID"];
            }
            break;
        case 'searchBook':
            $template->setTemplate($adminDir . "books.html");
            $template->results = $_SESSION["results"];
            break;
        case 'books':
            $template->results = getListBooks();
            break;
        case 'deleteAuthor':
            if (isset($_GET["authorID"])) {
                deleteAuthor($_GET["authorID"]);
            }
            $template->setTemplate($adminDir . "authors.html");
            $template->results = getListAuthors();
            break;
        case 'editAuthor':
            if (isset($_GET["authorID"])) {
                $template->author = getAuthor();
                $_SESSION["authorID"] = $_GET["authorID"];
            }
            break;
        case 'searchAuthor':
            $template->setTemplate($adminDir . "authors.html");
            $template->results = $_SESSION["results"];
            break;
        case 'authors':
            $template->results = getListAuthors();
            break;
        case 'comments':
        case 'deleteUser':
            if (isset($_GET["userID"])) {
                deleteUser($_GET["userID"]);
            }
            $template->setTemplate($adminDir ."users.html");
            $template->users = getListUsers();
            break;
        case 'searchUser':
            $template->setTemplate($adminDir . "users.html");
            $template->users = $_SESSION["results"];
            break;
        case 'users':
            $template->users = getListUsers();
            break;

    }
}
try {
    echo $template->execute();
} catch (Exception $e) {
    echo $e;
}

